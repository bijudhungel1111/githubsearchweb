# **[Github Search](https://bijugithubsearch.netlify.app/)**

Github Search is the web application which allow user to search users and repositories of github without logging in to the github account.

![GithubSearch](./public/images/git.png)
![GithubSearch](./public/images/git1.png)
![GithubSearch](./public/images/git2.png)
![GithubSearch](./public/images/git3.png)
![GithubSearch](./public/images/git4.png)
![GithubSearch](./public/images/git5.png)

## Installation

To run it locally;

```bash
  git clone https://gitlab.com/bijudhungel1111/githubsearchweb.git
  cd githubsearchweb
  git checkout master
  yarn or npm install
  yarn start or npm start
```

✅ It is done!

## Api Used On this project

### Github V3 API Reference

```base url
base url: https://api.github.com
```

#### Get users list

```http
  GET /search/users
```

| Parameter  | Type     | Description                                  |
| :--------- | :------- | :------------------------------------------- |
| `q`        | `string` | **Required**                                 |
| `page`     | `number` | **Optional (1,2,3)**                         |
| `per_page` | `number` | **Optional (10,25,50)**                      |
| `order`    | `string` | **Optional (asc,desc)**                      |
| `sort`     | `string` | **Optional (joined,followers,repositories)** |

#### Get Repos list

```http
  GET /search/repositories
```

| Parameter  | Type     | Description                        |
| :--------- | :------- | :--------------------------------- |
| `q`        | `string` | **Required (react native)**        |
| `page`     | `number` | **Optional (1,2,3)**               |
| `per_page` | `number` | **Optional (10,25,50)**            |
| `order`    | `string` | **Optional (asc,desc)**            |
| `sort`     | `string` | **Optional (stars,forks,updated)** |

#### Get Repository Details

```http
  GET /repos/${payload.fullName}
```

| Payload    | Type     | Description                 |
| :--------- | :------- | :-------------------------- |
| `fullName` | `string` | **Required (react native)** |

#### Get User Details

```http
  GET /users/${payload.user}
```

| Payload | Type     | Description                    |
| :------ | :------- | :----------------------------- |
| `user`  | `string` | **Required (bijudhungel1111)** |

#### Get User Repositories

```http
  GET /users/${payload.user}/repos
```

| Payload | Type     | Description                    |
| :------ | :------- | :----------------------------- |
| `user`  | `string` | **Required (bijudhungel1111)** |

#### Get User Repository Details

```http
  GET /repos/${payload.user}/${payload.repoName}
```

| Payload    | Type     | Description                    |
| :--------- | :------- | :----------------------------- |
| `user`     | `string` | **Required (bijudhungel1111)** |
| `repoName` | `string` | **Required (githubsearchweb)** |

## Packages that are used in this project

- axios
- lodash
- moment
- react-icons
- react-paginate
- react-redux
- redux
- redux-thunk
- sass
- react-router-dom
- react-toastify

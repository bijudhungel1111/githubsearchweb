import React from "react";
import { Routes, Route } from "react-router-dom";

import Home from "../pages/Home";
import ReposDetails from "../pages/ReposDetails";
import UserDetails from "../pages/UserDetails";
import UserFollowers from "../pages/UserFollowers";

const ScreenRoutes = () => {
  return (
    <Routes>
      <Route exact path="/" element={<Home />} />
      <Route path="/user/:name" element={<UserDetails />} />
      <Route exact path="/user/:name/followers" element={<UserFollowers />} />
      <Route path={`/repoDetails/:name`} element={<ReposDetails />} />
    </Routes>
  );
};
export default ScreenRoutes;

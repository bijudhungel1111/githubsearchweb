import _ from "lodash";
import Alert from "../component/Alert";
export const errorConsole = (error) => {
  if (error == "Error: Network Error") {
    Alert("error", "Check your network connection");
  } else if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    console.log("error.response.data", error.response.data);
    console.log("error.response.config.data", error.response.config.data);
    console.log("error.response.status", error.response.status);
    console.log("error.response.headers", error.response.headers);

    Alert(
      "error",
      !_.isUndefined(error.response.data.errors) &&
        !_.isArray(error.response.data.errors)
        ? error.response.data.errors
        : error.response.data.message
        ? error.response.data.message
        : error.response.data.error?.message
    );

    return;
  } else {
    // Something happened in setting up the request that triggered an Error
    console.log("Error", error.message);
    return;
  }
  console.log(error.config);
};

export const numberToKValue = (num) => {
  let result = num;
  if (num && num >= 1000) {
    result = `${Math.floor(num / 1000)}K`;
  }

  return result;
};

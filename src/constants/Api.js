import axios from "axios";
import { get } from "lodash";
import { errorConsole } from "./Helper";
axios.defaults.baseURL = "https://api.github.com";

export default class Api {
  /**
   *
   * @param {"orderItem":"des", "sortItem":"forks","page":1,"per_page":20,"searchInupt":"react-native"} payload
   */

  async getReposList(payload) {
    const {
      searchInput,
      page = 1,
      per_page = 10,
      sortItem = null,
      orderItem = "desc",
    } = payload;
    try {
      const res = await axios.get(`/search/repositories`, {
        params: {
          q: searchInput,
          sort: sortItem,
          order: orderItem,
          page: page,
          per_page: per_page,
        },
      });

      //console.log("response value", res);

      const data = get(res, "data");
      //console.log(data, "data from response");
      return data;
    } catch (e) {
      errorConsole(e);
      console.log("Error on ", e);
      throw e;
    }
  }
  /**
   *
   * @param {"fullName":"facebook/react-native"} payload
   */
  async getRepoReadMeDetails(payload) {
    try {
      const res = await axios.get(`/repos/${payload.fullName}/readme`, {
        headers: {
          Accept: "application/vnd.github.VERSION.html",
        },
      });

      //console.log("response value", res);

      const data = get(res, "data");
      //console.log(data, "data from response");
      return data;
    } catch (e) {
      console.log("Error on ", e);
      errorConsole(e);
      throw e;
    }
  }
  async getRepoDetails(payload) {
    try {
      const res = await axios.get(`/repos/${payload.fullName}`);

      // console.log("response value", res);

      const data = get(res, "data");
      ////console.log(data, "data from response");
      return data;
    } catch (e) {
      console.log("Error on ", e);
      errorConsole(e);
      throw e;
    }
  }
  /**
   *
   * @param {"page":1,"per_page":20,"searchInput":"bijudhungel","sortItem":"followers","order":"desc"} payload
   */
  async getUsersList(payload) {
    const {
      searchInput,
      page,
      per_page = 10,
      sortItem = null,
      orderItem = "desc",
    } = payload;
    try {
      const res = await axios.get(`/search/users`, {
        params: {
          q: searchInput,
          page: page,
          order: orderItem,
          sort: sortItem,
          per_page: per_page,
        },
      });

      //console.log("response value", res);

      const data = get(res, "data");
      //console.log(data, "data from response");
      return data;
    } catch (e) {
      errorConsole(e);
      console.log("Error on ", e);
      throw e;
    }
  }
  /**
   *
   * @param {"user":"bijudhungel"} payload
   */
  async getUserDetails(payload) {
    try {
      const res = await axios.get(`/users/${payload.user}`);

      // console.log("response value", res);

      const data = get(res, "data");
      //console.log(data, "data from response");
      return data;
    } catch (e) {
      console.log("Error on ", e);
      errorConsole(e);
      throw e;
    }
  }
  async getUserFollowersDetails(payload) {
    try {
      const res = await axios.get(`/users/${payload.user}/followers`);

      //console.log("response value", res);

      const data = get(res, "data");
      //console.log(data, "data from response");
      return data;
    } catch (e) {
      console.log("Error on ", e);
      errorConsole(e);
      throw e;
    }
  }
  async getUserReposDetails(payload) {
    try {
      const res = await axios.get(`/users/${payload.user}/repos`);

      //console.log("response value", res);

      const data = get(res, "data");
      //console.log(data, "data from response");
      return data;
    } catch (e) {
      console.log("Error on ", e);
      errorConsole(e);
      throw e;
    }
  }
  /**
   *
   * @param {"user":"bijudhungel"} payload
   */
  async getUsersRepos(payload) {
    try {
      const res = await axios.get(`/users/${payload.user}/repos`);

      // console.log("response value", res);

      const data = get(res, "data");
      //console.log(data, "data from response");
      return data;
    } catch (e) {
      console.log("Error on ", e);
      errorConsole(e);
      throw e;
    }
  }
  /**
   *
   * @param {"orgName":"touchwa.re"} payload
   */
  async getOrganizationRepos(payload) {
    try {
      const res = await axios.get(`/orgs/${payload.orgName}/repos`);

      // console.log("response value", res);

      const data = get(res, "data");
      //console.log(data, "data from response");
      return data;
    } catch (e) {
      console.log("Error on ", e);
      throw e;
    }
  }
  /**
   *
   * @param {"user":"bijudhungel11","repoName":"sai-shiksha-niketan"} payload
   */
  async getUserReposDetails(payload) {
    try {
      const res = await axios.get(`/repos/${payload.user}/${payload.repoName}`);

      //console.log("response value", res);

      const data = get(res, "data");
      //console.log(data, "data from response");
      return data;
    } catch (e) {
      console.log("Error on ", e);
      throw e;
    }
  }
}

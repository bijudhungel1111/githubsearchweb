import React from "react";
import ReactPaginate from "react-paginate";

import "./style.scss";
const Pagination = ({ onPageChange, pageCount }) => {
  return (
    <div className="paginationContainer">
      <ReactPaginate
        nextLabel="next >"
        onPageChange={onPageChange}
        pageCount={pageCount}
        pageRangeDisplayed={3}
        marginPagesDisplayed={1}
        previousLabel="< prev"
        pageClassName="page-item"
        pageLinkClassName="page-link"
        previousClassName="page-item"
        previousLinkClassName="page-link"
        nextClassName="page-item"
        nextLinkClassName="page-link"
        breakLabel="......."
        breakClassName="page-item"
        breakLinkClassName="page-link"
        containerClassName="pagination"
        activeClassName="active"
        renderOnZeroPageCount={null}
      />
    </div>
  );
};

export default Pagination;

import React from "react";
import "./style.scss";
const Loader = () => {
  return (
    <div className="loader">
      {"biju".split("").map((item, index) => {
        return <div className="bubble" key={index}></div>;
      })}
    </div>
  );
};

export default Loader;

import { toast, ToastContainer } from "react-toastify";
import { allColor } from "../../constants";

const Alert = (
  type = "error",
  message = "Invalid Data",
  color = allColor.error
) => {
  return toast[type](message);
};

export default Alert;

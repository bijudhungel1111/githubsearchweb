import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { AiFillGithub } from "react-icons/ai";
import { RiGitRepositoryLine } from "react-icons/ri";
import { FiUsers } from "react-icons/fi";
import { Link } from "react-router-dom";

import "./style.scss";

import { getUserDetails } from "../../pages/Home/Action";
const UserCard = ({ avatar, userName, userGitName, gitUrl }) => {
  const dispatch = useDispatch();
  const store = useSelector((state) => {
    return { blank: state.blank };
  });

  const iconList = [
    {
      icon: <AiFillGithub />,
      url: gitUrl,
      link: true,
    },
    {
      icon: <RiGitRepositoryLine />,
      url: `/user/${userName}`,
      data: "repos",
    },
    {
      icon: <FiUsers />,
      url: `/user/${userName}`,
      data: "followers",
    },
  ];
  const Icon = ({ icon }) => {
    return (
      <div className="icon">
        <span>{icon}</span>
      </div>
    );
  };

  const clickHandler = () => {
    /* const payload = {
      user: userGitName,
    };
    dispatch(getUserDetails(payload)); */
  };
  return (
    <div>
      <div className="userCard">
        <Link className="image" to={`/user/${userName}`} onClick={clickHandler}>
          <img src={avatar} alt={userName} />
        </Link>
        <div>
          <Link to={`/user/${userName}`} className="userName">
            <h1>{userName}</h1>
          </Link>
          <div className="iconContainer">
            {iconList.map((item, index) => {
              return (
                <div key={index}>
                  {item.link ? (
                    <a
                      href={item.url}
                      onClick={(e) => {
                        e.preventDefault();
                        window.open(item.url, "");
                      }}
                    >
                      <Icon icon={item.icon} />
                    </a>
                  ) : (
                    <Link
                      state={item.data}
                      to={{
                        pathname: item.url,
                      }}
                    >
                      <Icon icon={item.icon} />
                    </Link>
                  )}
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default UserCard;

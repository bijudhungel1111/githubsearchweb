import React from "react";
import "./style.scss";
const FollowerCard = ({ avatar, userName }) => {
  return (
    <div className="followerCard">
      <img src={avatar} />
      <h2>{userName}</h2>
    </div>
  );
};

export default FollowerCard;

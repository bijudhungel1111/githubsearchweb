import React from "react";
import { AiOutlineStar, AiOutlineEye } from "react-icons/ai";
import { RiGitRepositoryLine } from "react-icons/ri";
import { GrFormView } from "react-icons/gr";
import { FiUsers } from "react-icons/fi";
import { Link, useNavigate } from "react-router-dom";
import moment from "moment";

import "./style.scss";

const RepoCard = ({
  fullName,
  gitUrl,
  forks,
  viewers,
  description,
  stars,
  language,
  license,
  updatedAt,
  issues,
  sponser = false,
}) => {
  const navigate = useNavigate();
  const repoClickHandler = () => {
    navigate(`/repoDetails/${fullName.split("/").join(",")}`, {
      replace: false,
    });
  };

  return (
    <Link to={`/repoDetails/${fullName.split("/").join(",")}`}>
      <div className="reposCard">
        <div className="name">
          <span>
            <RiGitRepositoryLine />
          </span>
          <a href={gitUrl}>
            <h1>{fullName}</h1>
          </a>
        </div>

        <p className="desc">{description}</p>
        <div className="bottomContainer">
          <div className="stars">
            <span>
              <AiOutlineStar />
            </span>
            <p>{stars}</p>
          </div>
          <div className="language">
            <p>{language}</p>
          </div>
          <div className="stars">
            <span>
              <AiOutlineEye />
            </span>
            <p>{viewers}</p>
          </div>
          <p>{license?.name}</p>
          <p>Updated {moment(updatedAt).endOf("day").fromNow()}</p>
        </div>
      </div>
    </Link>
  );
};

export default RepoCard;

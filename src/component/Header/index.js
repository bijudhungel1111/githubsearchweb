import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { BsFilterRight } from "react-icons/bs";
import _ from "lodash";

import { searchTextAction } from "../../pages/Home/Action";
import "./style.scss";

import Alert from "../Alert";

const Header = ({ showSidebar, setShowSideBar }) => {
  const dispatch = useDispatch();
  const store = useSelector((state) => {
    return { blank: state.blank, getSearchItems: state.getSearchItems };
  });
  const { isFetching } = store.getSearchItems;

  const [searchInput, setSearchInput] = useState("");

  const onSearchHandler = (e) => {
    e.preventDefault();
    if (_.isEmpty(searchInput)) {
      Alert("error", "Enter Some text to Search");
    } else {
      const payload = {
        searchInput,
      };
      dispatch(searchTextAction(payload));
    }
  };
  return (
    <div className="header">
      <div className={`header__sub1 ${showSidebar ? "active" : ""}`}>
        <Link to="/">
          <div className="header__logo">
            Github <span> Search</span>
          </div>
        </Link>
        <div
          style={{
            color: "white",
          }}
          onClick={() => {
            setShowSideBar((prev) => !prev);
          }}
        >
          <span className={`icon ${showSidebar ? "active" : ""} `}>
            <BsFilterRight />
          </span>
        </div>
      </div>
      <form className="header__search">
        <input
          onChange={(e) => {
            setSearchInput(e.target.value);
          }}
          className="header__input"
          placeholder="Enter a text to search"
          name="search"
        />
        <button
          type="submit"
          className="header__searchBtn"
          onClick={onSearchHandler}
        >
          {isFetching ? "Searching..." : "Search"}
        </button>
      </form>
    </div>
  );
};

export default Header;

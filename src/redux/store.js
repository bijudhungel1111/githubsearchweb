import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import thunk from "redux-thunk";

import getSearchItems from "../pages/Home/Reducer";

const initialState = {};
const reducer = combineReducers({
  getSearchItems,
});

/* to make the redux dev tools works */
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

/* creating the store with reducer and initailState and  middleware */
const store = createStore(
  reducer,
  initialState,
  composeEnhancer(applyMiddleware(thunk))
);

export default store;

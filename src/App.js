import { BrowserRouter } from "react-router-dom";
import "react-toastify/dist/ReactToastify.min.css";
import { toast, ToastContainer } from "react-toastify";

import Routes from "./routes/index";
import "./App.css";
function App() {
  return (
    <BrowserRouter>
      <div className="toastContainer">
        <ToastContainer
          style={styles.toastContainer}
          position="top-right"
          autoClose={2000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          theme="colored"
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
      </div>
      <Routes />
    </BrowserRouter>
  );
}

export const styles = {
  toastContainer: { fontSize: "1.6rem", fontWeight: "bold", zIndex: 11111111 },
};
export default App;

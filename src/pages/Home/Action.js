import Api from "../../constants/Api";

const myApi = new Api();
export const searchTextAction = (payload) => async (dispatch) => {
  console.log(payload);
  dispatch({
    type: "GET_SEARCH_ITEMS_FETCHING_DATA_ATTEMPT",
  });
  try {
    const data = await myApi.getReposList(payload);
    const data1 = await myApi.getUsersList(payload);
    dispatch({
      type: "GET_SEARCH_ITEMS_FETCHING_DATA_SUCCESS",
      payload: {
        repos: data,
        users: data1,
        defaultValue: {
          ...payload,
        },
        searchInput: payload.searchInput,
      },
    });
  } catch (error) {
    dispatch({
      type: "GET_SEARCH_ITEMS_FETCHING_DATA_FAILURE",
      payload: error,
    });
  }
};
export const searchReposAction = (payload) => async (dispatch) => {
  console.log(payload);
  dispatch({
    type: "GET_REPOS_FETCHING_DATA_ATTEMPT",
  });
  try {
    const data = await myApi.getReposList(payload);
    dispatch({
      type: "GET_REPOS_FETCHING_DATA_SUCCESS",
      payload: {
        repos: data,
        defaultValue: {
          ...payload,
        },
        searchInput: payload.searchInput,
      },
    });
  } catch (error) {
    dispatch({
      type: "GET_REPOS_FETCHING_DATA_FAILURE",
      payload: error,
    });
  }
};
export const searchUserAction = (payload) => async (dispatch) => {
  console.log(payload);
  dispatch({
    type: "GET_USERS_FETCHING_DATA_ATTEMPT",
  });
  try {
    const data1 = await myApi.getUsersList(payload);
    dispatch({
      type: "GET_USERS_FETCHING_DATA_SUCCESS",
      payload: {
        users: data1,
        defaultValue: {
          ...payload,
        },
        searchInput: payload.searchInput,
      },
    });
  } catch (error) {
    dispatch({
      type: "GET_USERS_FETCHING_DATA_FAILURE",
      payload: error,
    });
  }
};
export const getRepoDetails = (payload) => async (dispatch) => {
  console.log(payload);
  dispatch({
    type: "GET_REPO_DETAILS_FETCHING_DATA_ATTEMPT",
  });
  try {
    const data1 = await myApi.getRepoDetails(payload);
    const readme = await myApi.getRepoReadMeDetails(payload);
    dispatch({
      type: "GET_REPO_DETAILS_FETCHING_DATA_SUCCESS",
      payload: {
        details: data1,
        readme,
        defaultValue: {
          ...payload,
        },
        searchInput: payload.searchInput,
      },
    });
  } catch (error) {
    dispatch({
      type: "GET_REPO_DETAILS_FETCHING_DATA_FAILURE",
      payload: error,
    });
  }
};

export const getUserDetails = (payload) => async (dispatch) => {
  console.log(payload);
  dispatch({
    type: "GET_USER_DETAILS_FETCHING_DATA_ATTEMPT",
  });
  try {
    const details = await myApi.getUserDetails(payload);
    const followers = await myApi.getUserFollowersDetails(payload);
    const repos = await myApi.getUsersRepos(payload);

    dispatch({
      type: "GET_USER_DETAILS_FETCHING_DATA_SUCCESS",
      payload: {
        details,
        followers,
        repos,
      },
    });
  } catch (error) {
    dispatch({
      type: "GET_USER_DETAILS_FETCHING_DATA_FAILURE",
      payload: error,
    });
  }
};

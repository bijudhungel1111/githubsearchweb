let initialState = {
  response: {
    defaultValue: {
      page: 1,
      per_page: 20,
      sortItem: "",
      searcInput: "",
      orderItem: "",
    },
    repos: {
      total_count: 0,
      incomplete_results: false,
      items: [],
    },
    users: {
      total_count: 0,
      incomplete_results: false,
      items: [],
    },
  },
  repoDetails: {
    details: {},
    readme: "",
  },
  userDetails: {
    details: {
      login: "",
      id: "",
      node_id: "",
      avatar_url: "",
      gravatar_id: "",
      url: "",
      html_url: "",
      followers_url: "",
      following_url: "",
      gists_url: "",
      starred_url: "",
      subscriptions_url: "",
      organizations_url: "",
      repos_url: "",
      events_url: "",
      received_events_url: "",
      type: "",
      site_admin: "",
      name: "",
      company: "",
      blog: "",
      location: "",
      email: "",
      hireable: "",
      bio: "",
      twitter_username: "",
      public_repos: "",
      public_gists: "",
      followers: "",
      following: "",
      created_at: "",
      updated_at: "",
    },
    followers: [],
    repos: [],
  },
  isFetching: false,
  isError: false,
};

export default function Reducer(state = initialState, action) {
  switch (action.type) {
    case "GET_SEARCH_ITEMS_FETCHING_DATA_SUCCESS":
      return {
        ...state,
        response: {
          defaultValue: {
            ...state.response.defaultValue,
            ...action.payload.defaultValue,
          },
          ...action.payload,
        },
        isFetching: false,
        isError: false,
      };
    case "GET_SEARCH_ITEMS_FETCHING_DATA_FAILURE":
      return {
        ...state,
        isError: action.payload,
        isFetching: false,
      };
    case "GET_SEARCH_ITEMS_FETCHING_DATA_ATTEMPT":
      return {
        ...state,
        isFetching: true,
        isError: false,
      };
    case "GET_REPOS_FETCHING_DATA_SUCCESS":
      return {
        ...state,
        response: {
          ...state.response,
          defaultValue: {
            ...state.response.defaultValue,
            ...action.payload.defaultValue,
          },
          ...action.payload,
        },
        isFetching: false,
        isError: false,
      };
    case "GET_REPOS_FETCHING_DATA_FAILURE":
      return {
        ...state,
        isError: action.payload,
        isFetching: false,
      };
    case "GET_REPOS_FETCHING_DATA_ATTEMPT":
      return {
        ...state,
        isFetching: true,
        isError: false,
      };
    case "GET_USERS_FETCHING_DATA_SUCCESS":
      return {
        ...state,

        response: {
          ...state.response,
          defaultValue: {
            ...state.response.defaultValue,
            ...action.payload.defaultValue,
          },
          ...action.payload,
        },
        isFetching: false,
        isError: false,
      };
    case "GET_USERS_FETCHING_DATA_FAILURE":
      return {
        ...state,
        isError: action.payload,
        isFetching: false,
      };
    case "GET_USERS_FETCHING_DATA_ATTEMPT":
      return {
        ...state,
        isFetching: true,
        isError: false,
      };
    case "GET_REPO_DETAILS_FETCHING_DATA_SUCCESS":
      return {
        ...state,
        repoDetails: action.payload,
        isFetching: false,
        isError: false,
      };
    case "GET_REPO_DETAILS_FETCHING_DATA_FAILURE":
      return {
        ...state,
        isError: action.payload,
        isFetching: false,
      };
    case "GET_REPO_DETAILS_FETCHING_DATA_ATTEMPT":
      return {
        ...state,
        isFetching: true,
        isError: false,
      };
    case "GET_USER_DETAILS_FETCHING_DATA_SUCCESS":
      return {
        ...state,
        userDetails: action.payload,
        isFetching: false,
        isError: false,
      };
    case "GET_USER_DETAILS_FETCHING_DATA_FAILURE":
      return {
        ...state,
        isError: action.payload,
        isFetching: false,
      };
    case "GET_USER_DETAILS_FETCHING_DATA_ATTEMPT":
      return {
        ...state,
        userDetails: {
          details: {
            login: "",
            id: "",
            node_id: "",
            avatar_url: "",
            gravatar_id: "",
            url: "",
            html_url: "",
            followers_url: "",
            following_url: "",
            gists_url: "",
            starred_url: "",
            subscriptions_url: "",
            organizations_url: "",
            repos_url: "",
            events_url: "",
            received_events_url: "",
            type: "",
            site_admin: "",
            name: "",
            company: "",
            blog: "",
            location: "",
            email: "",
            hireable: "",
            bio: "",
            twitter_username: "",
            public_repos: "",
            public_gists: "",
            followers: "",
            following: "",
            created_at: "",
            updated_at: "",
          },
          followers: [],
          repos: [],
        },
        isFetching: true,
        isError: false,
      };
    case "RESET_ALL_LOADING":
      return {
        ...state,
        isFetching: false,
        isError: false,
      };

    default:
      return state;
  }
}

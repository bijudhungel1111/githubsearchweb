import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import "./style.scss";

import { searchReposAction, searchUserAction } from "./Action";

import Header from "../../component/Header";
import UserCard from "../../component/Card/UserCard";
import RepoCard from "../../component/Card/RepoCard";
import Loader from "../../component/Loader";
import Pagination from "../../component/Pagination";

const Home = () => {
  const dispatch = useDispatch();
  const store = useSelector((state) => {
    return { blank: state.blank, getSearchItems: state.getSearchItems };
  });
  const { users, repos } = store.getSearchItems.response;

  const [showSidebar, setShowSideBar] = useState(false);
  const [clickValue, setClickValue] = useState("repo");
  const [userPageSize, setUserPageSize] = useState(30);
  const [reposPageSize, setReposPageSize] = useState(20);

  const onSortChangeHandler = (e) => {
    const { value } = e.target;

    const payload = {
      ...store.getSearchItems.response.defaultValue,
      sortItem: value.split("/")[0],
      orderItem: value.split("/")[1],
    };
    if (clickValue === "repo") {
      dispatch(searchReposAction(payload));
    } else {
      dispatch(searchUserAction(payload));
    }
  };
  const onUserPageSizeChangeHandler = (e) => {
    const { value: per_page } = e.target;
    setUserPageSize(per_page);
    const payload = {
      ...store.getSearchItems.response.defaultValue,
      per_page,
    };

    dispatch(searchUserAction(payload));
  };
  const onRepoPageSizeChangeHandler = (e) => {
    const { value: per_page } = e.target;
    setReposPageSize(per_page);
    const payload = {
      ...store.getSearchItems.response.defaultValue,

      per_page,
    };
    dispatch(searchReposAction(payload));
  };
  const onUserPageChangeHandler = (page) => {
    const payload = {
      ...store.getSearchItems.response.defaultValue,
      page,
    };

    dispatch(searchUserAction(payload));
  };
  const onRepoPageChangeHandler = (page) => {
    const payload = {
      ...store.getSearchItems.response.defaultValue,
      page,
    };
    dispatch(searchReposAction(payload));
  };
  return (
    <>
      <div className="homescreen">
        <Header showSidebar={showSidebar} setShowSideBar={setShowSideBar} />
        <div className={`homescreen__sidebar ${showSidebar ? "active" : ""}`}>
          <div
            className={`homescreen__sidebar--main ${
              showSidebar ? "active" : ""
            }`}
          >
            <div className={`sub ${showSidebar ? "active" : ""}`}>
              <div
                className={`homescreen__sidebar--sub1  ${
                  showSidebar ? "active" : ""
                }`}
              >
                <div
                  onClick={() => setClickValue("user")}
                  className={`${clickValue === "user" ? "active" : ""}`}
                >
                  User {users.total_count > 0 ? users.total_count : ""}
                </div>
                <div
                  onClick={() => setClickValue("repo")}
                  className={`${clickValue === "repo" ? "active" : ""}`}
                >
                  Repositories {repos.total_count > 0 ? repos.total_count : ""}
                </div>
              </div>
              <div
                className={`homescreen__sidebar--sub2  ${
                  showSidebar ? "active" : ""
                }`}
              >
                <span> Sort By</span>
                <select
                  defaultValue={"Sort By"}
                  onChange={onSortChangeHandler}
                  disabled={
                    (clickValue === "repo" && repos.total_count > 10) ||
                    (clickValue !== "repo" && users.total_count > 10)
                      ? false
                      : true
                  }
                >
                  {clickValue === "repo" ? (
                    <>
                      <option value="/desc">Best match</option>
                      <option value="stars/desc">Most Stars</option>
                      <option value="stars/asc">Fewest Stars</option>
                      <option value="forks/desc">Most forks</option>
                      <option value="forks/asc">Fewest forks</option>
                      <option value="updated/desc">Recently updated</option>
                      <option value="updated/asc">
                        Least recently updated
                      </option>
                    </>
                  ) : (
                    <>
                      <option value="/desc">Best match</option>
                      <option value="followers/desc">Most followers</option>
                      <option value="followers/asc">Fewest followers</option>
                      <option value="joined/desc">Most recently joined</option>
                      <option value="joined/asc">Least recently joined</option>
                      <option value="repositories/desc">
                        Most repositories
                      </option>
                      <option value="repositories/asc">
                        Fewest repositories
                      </option>
                    </>
                  )}
                </select>
              </div>
              <div
                className={`homescreen__sidebar--sub3  ${
                  showSidebar ? "active" : ""
                }`}
              >
                <span>Page Size</span>
                <select
                  disabled={
                    users.total_count > 10 || repos.total_count > 10
                      ? false
                      : true
                  }
                  onChange={
                    clickValue === "repo"
                      ? onRepoPageSizeChangeHandler
                      : onUserPageSizeChangeHandler
                  }
                  value={clickValue === "repo" ? reposPageSize : userPageSize}
                >
                  <option value="10">10</option>
                  <option value="25">25</option>
                  <option value="50">50</option>
                </select>
              </div>
            </div>
          </div>
        </div>

        {store.getSearchItems.isFetching && <Loader />}

        <div className="homescreen__cards">
          {clickValue === "user" && (
            <>
              <div className="users">
                {users.items.map((item, index) => {
                  return (
                    <UserCard
                      gitUrl={item.html_url}
                      userName={item.login}
                      avatar={item.avatar_url}
                      userGitName={item.login}
                      key={index}
                    />
                  );
                })}
              </div>
              {users?.total_count > 10 && (
                <Pagination
                  onPageChange={(e) => {
                    onUserPageChangeHandler(e.selected);
                  }}
                  pageCount={users?.total_count / userPageSize}
                />
              )}
            </>
          )}
          {clickValue === "repo" && (
            <>
              <div className="repos">
                <>
                  {repos.items.map((item, index) => {
                    return (
                      <RepoCard
                        key={index}
                        stars={item.stargazers_count}
                        fullName={item.full_name}
                        description={item.description}
                        gitUrl={item.html_url}
                        license={item.license}
                        language={item.language}
                        viewers={item.watchers}
                        updatedAt={item.updated_at}
                        forks={item.forks}
                        cloneUrl={item.clone_url}
                        issues={item.open_issues_count}
                      />
                    );
                  })}
                </>
              </div>

              {repos.total_count > 20 && (
                <Pagination
                  onPageChange={(e) => {
                    onRepoPageChangeHandler(e.selected);
                  }}
                  pageCount={repos.total_count / reposPageSize}
                />
              )}
            </>
          )}
        </div>
      </div>
    </>
  );
};

export default Home;

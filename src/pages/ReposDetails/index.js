import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { getRepoDetails } from "../Home/Action";
import { RiGitRepositoryLine } from "react-icons/ri";
import { GiScales } from "react-icons/gi";
import { BiLink, BiGitRepoForked } from "react-icons/bi";
import { AiOutlineStar, AiOutlineEye } from "react-icons/ai";

import "./style.scss";

import Loader from "../../component/Loader";
import { numberToKValue } from "../../constants/Helper";
const ReposDetails = () => {
  const dispatch = useDispatch();
  const store = useSelector((state) => {
    return { blank: state.blank, getSearchItems: state.getSearchItems };
  });
  const { isFetching } = store.getSearchItems;
  const { details, readme } = store.getSearchItems.repoDetails;
  const raw = readme;

  const params = useParams();

  useEffect(() => {
    console.log(params);
    dispatch(
      getRepoDetails({
        fullName: params?.name?.split(",").join("/"),
      })
    );
  }, []);

  const iconWithText = (icon, text, link = "") => {
    return (
      <div className="iconWithText">
        <span>{icon}</span>
        {link !== "" ? (
          <a href={link}>
            <h2>{link}</h2>
          </a>
        ) : (
          <h2>{text}</h2>
        )}
      </div>
    );
  };
  return (
    <div className="repoDetails">
      {isFetching ? (
        <div className="repoDetails__loader">
          <Loader />
        </div>
      ) : (
        <div>
          <div className="repoDetails__main">
            <div className="name">
              <span>
                <RiGitRepositoryLine />
              </span>
              <a href={details.html_url}>
                <h1>{details?.full_name}</h1>
              </a>
            </div>
            <div className="repoDetails__main--part">
              <div className="abouts">
                <h1>About</h1>

                <div>
                  <p>{details.description}</p>

                  {iconWithText(
                    <BiLink />,
                    details?.homepage,
                    details?.homepage
                  )}
                  {details.license?.name &&
                    iconWithText(<GiScales />, details.license.name)}
                  {iconWithText(
                    <AiOutlineStar />,
                    ` ${numberToKValue(details?.stargazers_count)} stars`
                  )}
                  {iconWithText(
                    <AiOutlineEye />,
                    `${numberToKValue(details?.subscribers_count)} watching`
                  )}
                  {iconWithText(
                    <BiGitRepoForked />,
                    `${numberToKValue(details?.forks_count)} forks`
                  )}
                </div>
              </div>
            </div>
          </div>
          <div>
            <h2 className="readmeHeading">Readme.md</h2>
            <div
              className="readme"
              dangerouslySetInnerHTML={{
                __html: raw,
              }}
            ></div>
          </div>
        </div>
      )}
    </div>
  );
};

export default ReposDetails;

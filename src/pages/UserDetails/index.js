import React, { useEffect, useState } from "react";
import { AiFillGithub } from "react-icons/ai";
import { GoLocation } from "react-icons/go";
import { FiUsers } from "react-icons/fi";
import { useDispatch, useSelector } from "react-redux";
import { useParams, useLocation, Link } from "react-router-dom";
import _ from "lodash";

import "./style.scss";

import { getUserDetails } from "../Home/Action";

import UserCard from "../../component/Card/UserCard";
import Loader from "../../component/Loader";
import RepoCard from "../../component/Card/RepoCard";
const UserDetails = () => {
  const dispatch = useDispatch();
  const store = useSelector((state) => {
    return { blank: state.blank, getSearchItems: state.getSearchItems };
  });
  const { isFetching } = store.getSearchItems;
  const { details, followers, repos } = store.getSearchItems.userDetails;

  const params = useParams();
  const location = useLocation();
  const [active, setActive] = useState("repos");

  useEffect(() => {
    if (location.state) {
      setActive(location.state);
    }
    const payload = {
      user: params.name,
    };
    dispatch(getUserDetails(payload));
  }, [params?.name]);

  const iconList = [
    {
      icon: <AiFillGithub />,
      url: details.html_url,
      data: details.html_url,
      link: true,
    },
    {
      icon: <GoLocation />,
      url: `/user/${details.login}`,
      data: details.location,
    },
    {
      data1: "followers",
      icon: <FiUsers />,
      url: `/user/${details.login}`,
      data: `${
        details.followers > 1000
          ? `${Math.floor(details.followers / 1000)}K`
          : details.followers
      } Followers, ${
        details.following > 1000
          ? `${Math.floor(details.following / 1000)}K`
          : details.following
      } Following`,
    },
  ];

  return (
    <>
      <div className="userDetails">
        <div className="userDetails__left">
          <div className="userDetails__left--main">
            <div className="userDetails__image">
              {isFetching ? (
                <div className="userDetails__avatar"></div>
              ) : (
                <img src={details.avatar_url} className="userDetails__avatar" />
              )}
            </div>
            <div className="userDetails__detail">
              <h2>{details.login}</h2>
              <h3>{details.bio}</h3>

              <div className="userDetails__detail--icons">
                {iconList.map((item, index) => {
                  return (
                    !_.isEmpty(item.data) &&
                    (item?.link ? (
                      <a
                        href={item.url}
                        className="icon"
                        onClick={(e) => {
                          e.preventDefault();
                          window.open(item.url, "");
                        }}
                      >
                        <span>{item.icon}</span> <p>{item.data}</p>
                      </a>
                    ) : (
                      <Link
                        to={item.url}
                        className="icon"
                        onClick={() => {
                          setActive("followers");
                        }}
                      >
                        <span>{item.icon}</span> <p>{item.data}</p>
                      </Link>
                    ))
                  );
                })}
              </div>
            </div>
          </div>
        </div>
        <div className="userDetails__right">
          <div className="userDetails__right--control">
            <h1
              onClick={() => setActive("repos")}
              className={`${active === "repos" ? "repos" : ""}`}
            >
              Repositories
            </h1>{" "}
            <h1
              onClick={() => setActive("followers")}
              className={`${active === "followers" ? "followers" : ""}`}
            >
              Followers
            </h1>
          </div>
          {isFetching ? (
            <div className="userDetails__loading">
              <Loader />
            </div>
          ) : (
            <div className="userDetails__right--main">
              {active === "repos" &&
                repos.map((item, index) => {
                  return (
                    <div>
                      <RepoCard
                        key={index}
                        stars={item.stargazers_count}
                        fullName={item.full_name}
                        description={item.description}
                        gitUrl={item.html_url}
                        license={item.license}
                        language={item.language}
                        viewers={item.watchers}
                        updatedAt={item.updated_at}
                        forks={item.forks}
                        cloneUrl={item.clone_url}
                        issues={item.open_issues_count}
                      />
                    </div>
                  );
                })}

              {active === "followers" && (
                <div className="followers">
                  {followers.map((item, index) => (
                    <UserCard
                      gitUrl={item.html_url}
                      userName={item.login}
                      avatar={item.avatar_url}
                      userGitName={item.login}
                      key={index}
                    />
                  ))}
                </div>
              )}
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default UserDetails;
